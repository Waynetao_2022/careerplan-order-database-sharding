package com.migrate.module.domain;

import lombok.Data;

import java.util.Date;

/**
 *
 * 数据源分片规则配置表
 *
 * @author zhonghuashishan
 */
@Data
public class ScrollShardConfig {

    private Long id;

    /**
     * 所属系统(会员、订单、交易)
     */
    private Long domainId;

    /**
     * 逻辑模型名(逻辑表或物理表名称)
     */
    private String logicModel;

    /**
     * 库分片列名称，多个列以逗号分隔
     */
    private String dbShardingColumns;

    /**
     * 表分片列名称，多个列以逗号分隔
     */
    private String tableShardingColumns;

    /**
     * 库分片策略类名
     */
    private String dbShardingAlgorithm;

    /**
     * 表分片策略类名
     */
    private String tableShardingAlgorithm;


}
