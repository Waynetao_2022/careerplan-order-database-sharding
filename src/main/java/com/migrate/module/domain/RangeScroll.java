package com.migrate.module.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 数据抽取模型
 *
 * @author zhonghuashishan
 */
@Data
public class RangeScroll implements Serializable {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 归属的业务系统名称
     */
    private String domain;
    /**
     * 归属的业务系统ID
     */
    private Long domainId;

    /**
     * 要写入的数据库表名
     */
    private String targetTableName;
    /**
     * 抽数的开始时间
     */
    private Date startTime;
    /**
     * 抽数的截止时间
     */
    private Date endTime;
    /**
     * 抽数起始查询字段值
     */
    private String startScrollId;
    /**
     * 抽数的字段名
     */
    private String scrollName;
    /**
     *  当前所属批次阶段号
     */
    private Integer curTicketStage;
    /**
     * 迁移批次
     */
    private String ticket;
    /**
     *  抽数指定的表
     */
    private String tableName;
    /**
     * 每页捞取数量
     */
    private Integer pageSize;
    /**
     * 是否重试
     */
    private Boolean retryFlag = false;
    /**
     * 已同步次数
     */
    private Integer retryTimes;

    /**
     *  进度类型(0滚动抽数,1核对抽数)
     */
    private Integer progressType;
}
