package com.migrate.module.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 迁移表
 *
 * @author zhonghuashishan
 */
@Data
public class EtlProgressReq implements Serializable {

    private static final long serialVersionUID = 3381526018986361684L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 业务系统名称
     */
    private String domain;

    /**
     * 业务系统ID
     */
    private Integer domainId;

    /**
     * 逻辑模型名（逻辑表）
     */
    private String logicModel;
    /**
     * 迁移批次
     */
    private String ticket;
    /**
     *  当前所属批次阶段号
     */
    private Integer curTicketStage;
    /**
     *  进度类型(0滚动抽数,1核对抽数)
     */
    private Integer progressType;
    /**
     *  迁移状态
     */
    private Integer status;

    /**
     * 开始滚动时间
     */
    private Date scrollTime;

}
