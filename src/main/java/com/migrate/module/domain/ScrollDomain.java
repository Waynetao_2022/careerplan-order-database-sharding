package com.migrate.module.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 滚动数据的业务对象配置表
 *
 * @author zhonghuashishan
 */
@Data
public class ScrollDomain implements Serializable {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 所属系统(会员、订单、交易)
     * 增量同步的表，是来源于哪个业务的，会员、订单、交易
     */
    private String domain;
    /**
     * 当数据源为来源的时候，配置对应的消息topic
     * 你要同步的表，表binlog被写入到了rocketmq的哪个topic里面去
     */
    private String domainTopic;
    /**
     * 数据源类型，1数据来源配置，2数据写入配置
     */
    private Integer dataSourceType;
    /**
     * 是否显示 shardingsphere sql执行日志
     */
    private Integer sqlshow;
    /**
     * 每个逻辑库中表的数量
     */
    private int tableNum;


}
