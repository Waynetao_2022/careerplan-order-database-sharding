package com.migrate.module.domain;

import lombok.Data;

import java.util.Date;

/**
 *
 * 迁移数据源配置表
 * @author zhonghuashishan
 */
@Data
public class ScrollDbConfig {

    private Long id;

    /**
     * 归属系统数据业务配置ID
     */
    private Long domainId;


    /**
     * 数据库连接串
     */
    private String jdbcUrl;

    /**
     * 用户名
     */
    private String jdbcUserName;

    /**
     * 密码
     */
    private String jdbcPassword;
}
