package com.migrate.module.domain;

import lombok.Data;

import java.util.Date;

/**
 * 迁移表数据统计表
 *
 * @author zhonghuashishan
 */
@Data
public class EtlStatistical {

    /**
     * 主键
     */
    private Long id;
    /**
     * 逻辑模型名（逻辑表）
     */
    private String logicModel;
    /**
     * 归属业务
     */
    private String domain;
    /**
     * 统计数据量
     */
    private Integer statisticalCount;
    /**
     * 统计时间
     */
    private Integer statisticalTime;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 查询统计开始时间
     */
    private Integer startTime;
    /**
     * 查询统计结束时间
     */
    private Integer endTime;


}
