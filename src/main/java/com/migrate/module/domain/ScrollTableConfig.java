package com.migrate.module.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 迁移表映射配置 将A库A表映射到B库B表
 *
 * @author zhonghuashishan
 */
@Data
public class ScrollTableConfig implements Serializable {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 所属系统(会员、订单、交易)
     */
    private String domain;
    /**
     * 源库表名
     */
    private String sourceTableName;
    /**
     * 目标库表名
     */
    private String targetTableName;

    private Date createTime;

    private Date updateTime;

}
