package com.migrate.module.mapper.migrate;

import com.migrate.module.domain.ScrollDbConfig;
import com.migrate.module.domain.ScrollDomain;
import com.migrate.module.domain.ScrollShardConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 同步数据源的相关配置
 *
 * @author zhonghuashishan
 */
public interface ScrollDbConfigMapper {
    /**
     * 查询当前配置的所有需要迁移的业务系统
     * @return
     */
    List<ScrollDomain> queryScrollDomainList();

    /**
     * 查询每个业务系统下的数据源配置
     * @param domainId 业务系统ID
     * @return
     */
    List<ScrollDbConfig> queryScrollDbConfigList(@Param("domainId") Long domainId);

    /**
     * 查询每个业务系统下的表对应的分库分表的策略配置
     * @param domainId 业务系统ID
     * @return
     */
    List<ScrollShardConfig> queryScrollShardConfigList(@Param("domainId") Long domainId);

    /**
     * 获取业务系统名称
     * @return
     */
    List<String> getDomainNames();

    /**
     * 通过domain名称获取旗下的表名
     * @param domain
     * @return
     */
    List<String> getTablesToDomain(@Param("domain") String domain);

    /**
     * 通过domain获取domainId
     * @param domain domain名称
     * @return domainId
     */
    Long getDomainId(@Param("domain") String domain);

    /**
     * 根据topic映射出对应的数据源库
     * @param topic
     * @return
     */
    ScrollDomain getDataBaseDomain(@Param("topic")String topic);

    /**
     * 通过源表名获取目标表名
     * @param domain         源业务系统
     * @param sourceTableName   正在同步的表名
     * @return
     */
    String getSourceTableName(@Param("domain") String domain,@Param("sourceTableName") String sourceTableName);
}
