package com.migrate.module.lock;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 锁竞争类对象
 *
 * @author zhonghuashishan
 */
public class PutBinlogLock {

    private final AtomicBoolean putMessageSpinLock = new AtomicBoolean(true);

    /**
     * 上锁
     */
    public void lock() {
        // 假设，多个线程同时来进行加锁
        boolean flag;
        do {
            // 多个线程都会去进行CAS操作，只有一个人可以把true变为false
            // atomic变量，默认就是支持线程安全性
            // 只有一个线程可以完成加锁的逻辑
            // flag = true
            // 其他线程来说，cas加锁都失败了，在进入自旋，flag是false
            flag = this.putMessageSpinLock.compareAndSet(true, false);
        }
        while (!flag);
    }

    /**
     * 解锁
     */
    public void unlock() {
        // 只有一个线程可以成功的执行cas，把false->true
        this.putMessageSpinLock.compareAndSet(false, true);
    }
}
