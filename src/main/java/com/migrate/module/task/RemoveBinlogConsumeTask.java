package com.migrate.module.task;

import com.migrate.module.mapper.migrate.EtlBinlogConsumeRecordMapper;
import com.migrate.module.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 定时任务移除掉已提交的增量同步消息
 *
 * @author zhonghuashishan
 */
@Slf4j
@Component
public class RemoveBinlogConsumeTask {

    /**
     * binlog消息同步消费记录表Mapper
     */
    @Autowired
    private EtlBinlogConsumeRecordMapper consumeRecordMapper;

    @Scheduled(cron = "0 0 0 1/1 * ? ")
    public void removeBinlogConsumeRecordTask(){
        // 每次删除超过当前时间5分钟的历史数据
        Date updateTime = DateUtils.addMinute(new Date(), -5);
        consumeRecordMapper.deleteCommittedConsumedRecords(updateTime);
        // 每隔5分钟把已经committed的消费记录做一个删除和清理
        // 每条binlog数据都会在你的库表里有一条消费记录，会导致这个数据太多了
    }
}
