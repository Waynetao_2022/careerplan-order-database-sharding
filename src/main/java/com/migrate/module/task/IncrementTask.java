package com.migrate.module.task;

import com.migrate.module.migrate.LocalQueue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 负责定时增量数据写入落地
 * 对于binlog，我们都是要定时批处理的，不是来一条binlog就处理一条
 * 默认的话，是收集15s内的数据统一做一个批处理
 *
 * @author zhonghuashishan
 */
@Slf4j
@Component
public class IncrementTask {

    /**
     * 负责增量数据的写入动作
     */
    @Scheduled(fixedDelay = 15000)
    void IncrementTask(){
        // 获取阻塞队列的方法
        LocalQueue localQueue = LocalQueue.getInstance();
        // 验证读队列的数据已被处理完毕
        if (!localQueue.getIsRead()) { // 刚开始默认就是false，如果说上一次数据导入操作，还在做
            log.info("增量数据执行写入");
            // 执行数据写入
            localQueue.doCommit();
        }
    }

}
