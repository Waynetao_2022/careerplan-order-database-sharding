package com.migrate.module.util;

import cn.hutool.json.JSONNull;

import java.util.*;

/**
 * 数据迁移工具类
 *
 * @author zhonghuashishan
 */
public class MigrateUtil {

    /**
     * 对集合中存在的空value值进行移除
     * @param mapList
     * @return
     */
    public static List <Map <String, Object>> removeNullValue(List <Map <String, Object>> mapList){
        for (Map <String, Object> map:mapList){
            removeNullValue(map);
        }
        return mapList;
    }

    /**
     * 更正Map中的Value空值对象类型
     * @param map
     */
    public static Map updateNullValue(Map map) {
        Set set = map.keySet();
        Map updateMap = new HashMap();
        for (Iterator iterator = set.iterator(); iterator.hasNext(); ) {
            Object obj = (Object) iterator.next();
            Object value = (Object) map.get(obj);

            if (value.equals(JSONNull.NULL)){
                updateMap.put(obj, null);
            } else {
                updateMap.put(obj, value);
            }
        }
        return updateMap;
    }
    /**
     * 移除map中的value空值
     * @param map
     * @return
     */
    private static void removeNullValue(Map map) {
        Set set = map.keySet();
        for (Iterator iterator = set.iterator(); iterator.hasNext(); ) {
            Object obj = (Object) iterator.next();
            Object value = (Object) map.get(obj);
            remove(value, iterator);
        }
    }


    /**
     * @param obj
     * @param iterator
     */
    private static void remove(Object obj,Iterator iterator){
        if (JSONNull.NULL.equals(obj)){
            iterator.remove();
        }
    }
}
