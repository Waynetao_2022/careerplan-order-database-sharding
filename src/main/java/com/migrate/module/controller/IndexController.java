package com.migrate.module.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.migrate.module.service.MigrateConfigService;
import com.migrate.module.service.MigrateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 首页controller
 *
 * @author zhonghuashishan
 */
@Slf4j
@Controller
@RequestMapping("/")
public class IndexController
{
    @Resource
    private MigrateService migrateService;

    @Resource
    MigrateConfigService migrateConfigService;

    /**
     * 跳转到首页
     * @return 首页html名称
     */
    @RequestMapping ("/toIndex")
    public String toIndex (HttpServletRequest request){
        List<String> scrollAbleTables = migrateService.getScrollAbleTables();
        List<String> domainNames = migrateConfigService.getDomainNames();
        if (CollUtil.isNotEmpty(scrollAbleTables))
        {
            request.setAttribute("scrollAbleTables", JSONUtil.toJsonStr(scrollAbleTables));
        }

        if (CollUtil.isNotEmpty(domainNames)){
            request.setAttribute("domainNames", JSONUtil.toJsonStr(domainNames));
        }
        return "index";
    }
}
