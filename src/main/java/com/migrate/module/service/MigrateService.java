package com.migrate.module.service;

import com.migrate.module.domain.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 数据迁移同步service
 *
 * @author zhonghuashishan
 */
public interface MigrateService {
    /**
     * 批量迁移数据到新库
     * @param scroll 数据对象
     * @param binLogs 要迁移的数据
     * @return 迁移结果
     */
    boolean migrateBat (RangeScroll scroll, List<BinLog> binLogs);

    /**
     * 根据唯一标识List查询所有符合要求的数据
     * @param scroll 数据对象
     * @param identifiers 唯一标识List
     * @param dbChannel 指向具体的BD库
     * @return 符合要求的数据
     */
    List<Map<String, Object>> findByIdentifiers(RangeScroll scroll, List<String> identifiers,String dbChannel);
    /**
     * 滚动拉取数据
     * @param rangeScroll 查询条件
     * @return 符合要求的数据
     */
    List <Map<String, Object>> queryInfoList (RangeScroll rangeScroll);

    /**
     * 补偿失败的范围滚动拉取数据
     * @param id
     * @param domain
     */
    void compensateRangeScroll(Long id,String domain);

    /**
     * 根据条件取得数据迁移记录
     * @param queryCondition 查询条件
     * @return 数据迁移记录
     */
    List <EtlProgress> getEtlProgresses (EtlProgress queryCondition);

    /**
     * 取得支持全量同步的表
     * @return 支持全量同步的表
     */
    List <String> getScrollAbleTables ();
    /**
     * 查询初始的订单号
     * @param rangeScroll
     * @return
     */
    String queryMinScrollId(RangeScroll rangeScroll);
}
