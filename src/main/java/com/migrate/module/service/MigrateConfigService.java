package com.migrate.module.service;

import com.migrate.module.domain.RangeScroll;
import com.migrate.module.domain.ScrollDbConfig;
import com.migrate.module.domain.ScrollDomain;
import com.migrate.module.domain.ScrollShardConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * 迁移配置获取
 * @author zhonghuashishan
 */
public interface MigrateConfigService {

    /**
     * 查询当前配置的所有需要迁移的业务系统
     * @return
     */
    List<ScrollDomain> queryScrollDomainList();

    /**
     * 查询每个业务系统下的数据源配置
     * @param domainId 业务系统ID
     * @return
     */
    List<ScrollDbConfig> queryScrollDbConfigList(Long domainId);

    /**
     * 查询每个业务系统下的表对应的分库分表的策略配置
     * @param domainId 业务系统ID
     * @return
     */
    List<ScrollShardConfig> queryScrollShardConfigList(Long domainId);

    /**
     * 获取业务系统名称列表
     * @return
     */
    List<String> getDomainNames();

    /**
     * 通过domain名称获取旗下的表名
     * @param domain domain名称
     * @return 表名
     */
    List<String> getTablesToDomain(String domain);

    /**
     * 通过domain获取domainId
     * @param domain domain名称
     * @return domainId
     */
    Long getDomainId(String domain);

    /**
     * 根据topic映射到具体的
     * @param topic
     * @return
     */
    RangeScroll getScrollConfig(String topic);

    /**
     * 通过源表名获取目标表名
     * @param domain            源业务系统
     * @param sourceTableName   正在同步的表名
     * @return
     */
    String getSourceTableName(@Param("domain") String domain, @Param("sourceTableName") String sourceTableName);

}
