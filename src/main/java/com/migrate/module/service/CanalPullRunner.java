package com.migrate.module.service;

import cn.hutool.core.collection.CollUtil;
import com.migrate.module.config.ApplicationContextUtil;
import com.migrate.module.domain.BinlogData;
import com.migrate.module.domain.EtlBinlogConsumeRecord;
import com.migrate.module.enumeration.BinlogType;
import com.migrate.module.enumeration.ConsumerStatus;
import com.migrate.module.mapper.migrate.EtlBinlogConsumeRecordMapper;
import com.migrate.module.migrate.LocalQueue;
import com.migrate.module.util.BinlogUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultLitePullConsumer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.Date;
import java.util.List;

/**
 * binlog消息拉取任务（只拉不提交）
 *
 * @author zhonghuashishan
 */
@Slf4j
public class CanalPullRunner implements Runnable
{
    /**
     * 消息主题
     */
    private final String topic;
    /**
     * rocketmq的nameServer地址
     */
    private final String nameServerUrl;
    /**
     * binlog消息同步消费记录表Mapper
     */
    private final EtlBinlogConsumeRecordMapper consumeRecordMapper;

    /**
     * 消息拉取任务构造方法
     * @param topic 消息主题
     * @param nameServerUrl rocketmq的nameServer地址
     */
    public CanalPullRunner(String topic, String nameServerUrl)
    {
        this.topic = topic;
        this.nameServerUrl = nameServerUrl;
        this.consumeRecordMapper = ApplicationContextUtil.getBean(EtlBinlogConsumeRecordMapper.class);
    }

    @Override
    public void run()
    {
        pullRun();
    }

    /**
     * 执行消息拉取
     */
    private void pullRun ()
    {
        try
        {
            DefaultLitePullConsumer litePullConsumer = new DefaultLitePullConsumer("binlogPullConsumer");
            litePullConsumer.setAutoCommit(false); // 设置了rocketmq consumer提交offset是关闭的，让他不要自动去提交offset，我们还没完全处理完毕
            // 此时offset已经被提交了，处理失败了，rocketmq不给我们再次重复消费了
            litePullConsumer.setNamesrvAddr(nameServerUrl);
            litePullConsumer.subscribe(topic, "*");
            litePullConsumer.start();

            try {
                while (true) {
                    // 拉取未消费消息
                    List<MessageExt> messageExts = litePullConsumer.poll();
                    if (CollUtil.isNotEmpty(messageExts))
                    {
                        for (MessageExt messageExt : messageExts)
                        {
                            byte[] body = messageExt.getBody();
                            String msg = new String(body);
                            // 记录queueId和offset
                            int queueId = messageExt.getQueueId();
                            long offset = messageExt.getQueueOffset();
                            String topic = messageExt.getTopic();

                            // topic、queue、offset、msg，四位一体，把所有的信息都拿到了
                            // 建议把rocketmq模块看一下

                            // 判断该消息是否已经存在消费记录，如果存在则跳过执行
                            EtlBinlogConsumeRecord existsRecord = consumeRecordMapper.getExistsRecord(queueId, offset,topic);
                            if (null == existsRecord)
                            {
                                // 新增消费记录
                                processNewMsg(messageExt, msg);
                            }
                            else
                            {
                                // 处理已经存在的消费记录
                                proccessExistsRecord(litePullConsumer, msg, existsRecord);
                            }
                        }
                    }
                    else
                    {
                        Thread.sleep(5000);
                    }
                }
            } finally {
                litePullConsumer.shutdown();
            }
        }
        catch (InterruptedException | MQClientException e)
        {
            try
            {
                // 假设要拉取消息的主题还不存在，则会抛出异常，这种情况下休眠五秒再重试
                Thread.sleep(5000);
                pullRun ();
            }
            catch (InterruptedException ignored)
            {

            }
        }
    }

    /**
     * 处理新的消息
     * @param messageExt mq消息对象
     * @param msg 消息内容
     */
    private void processNewMsg(MessageExt messageExt, String msg)
    {
        try
        {
            // 拿到的msg值一个字符串，字符串格式的binlog，但是我们需要对字符串格式的binlog做一个解析
            // 把这个解析后的binlog的信息封装到我们自定义的BinlogData对象里去
            BinlogData binlogData = BinlogUtils.getBinlogDataMap(msg);
            Boolean targetOperateType = BinlogType.INSERT.getValue().equals(binlogData.getOperateType())
                                || BinlogType.DELETE.getValue().equals(binlogData.getOperateType())
                                || BinlogType.UPDATE.getValue().equals(binlogData.getOperateType());
            if (!targetOperateType
                    || null == binlogData
                    || null == binlogData.getDataMap()
                    ){
                return;
            }

            // binlog消费处理记录，不要把binlog自己的数据放到里面去
            // 关闭rocketmq consumer自动提交offset的功能，每一条binlog都是异步处理的
            // 每一条binlog，都是提交到本地队列里去，依托队列，实现异步化的处理
            // consumer来说，如果人家异步化都没处理完毕呢，你就提交offset，说他已经处理成功了
            // 对于consumer来说，一条消息被先处理一下，把binlog再rocketmq里，所在的topic、queue、offset封装到record
            // 插入到db里去
            EtlBinlogConsumeRecord consumeRecord = new EtlBinlogConsumeRecord();
            consumeRecord.setQueueId(messageExt.getQueueId());
            consumeRecord.setOffset(messageExt.getQueueOffset());
            consumeRecord.setTopic(messageExt.getTopic());
            consumeRecord.setBrokerName(messageExt.getBrokerName());
            consumeRecord.setConsumeStatus(ConsumerStatus.NOT_CONSUME.getValue());
            consumeRecord.setCreateTime(new Date());
            consumeRecordMapper.insert(consumeRecord);

            // binlog数据会基于本地队列去提交，触发异步化的处理
            // 本条消息offset是不会提交给broker的，必须等到我异步化处理玩这个binlog之后
            // 才能去提交这条消息到broker里去
            LocalQueue.getInstance().submit(binlogData, consumeRecord);
        }
        catch (Exception e)
        {
            log.error("新增消费记录失败", e);
        }
    }

    /**
     * 处理已经存在的消费记录
     * @param litePullConsumer mq消费者
     * @param msg 消息内容
     * @param existsRecord 已经存在的消费记录
     */
    private void proccessExistsRecord(DefaultLitePullConsumer litePullConsumer, String msg, EtlBinlogConsumeRecord existsRecord)
    {
        // 已经存在的消费记录状态为已提交，说明mq里的对应消息修改提交状态失败了
        // （rocketmq源码里手动提交消息时，如果失败了只会记录日志不会抛出异常，因此这里必须再次尝试提交消息防止mq中未处理的消息和实际情况不符）
        try
        {
            if (ConsumerStatus.COMMITTED.getValue().equals(existsRecord.getConsumeStatus()))
            {
                litePullConsumer.seek(new MessageQueue(existsRecord.getTopic(), existsRecord.getBrokerName(), existsRecord.getQueueId()), existsRecord.getOffset());
                //这一步必须，不然手动提交的东西不对
                List<MessageExt> committedFaildmessageExts = litePullConsumer.poll();
                // 再次提交已消费的消息
                litePullConsumer.commitSync();
            }
            else
            {
                BinlogData binlogData = BinlogUtils.getBinlogDataMap(msg);
                if (null == binlogData){
                    return;
                }
                LocalQueue.getInstance().submit(binlogData, existsRecord);
            }
        }
        catch (Exception e)
        {
            log.error("消息重新消费失败", e);
        }
    }
}
