package com.migrate.module.migrate;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 处理动态SQL转换
 *
 * @author zhonghuashishan
 */
@Slf4j
public class PreparedStatementUtil {

    /**
     * SQL预处理，需要什么加什么
     * @param preparedStatement SQL预处理
     * @param pos 定位
     * @param value 值
     * @return
     */
    public static void buildPerParedStatement(PreparedStatement preparedStatement,int pos,Object value){
        try {
            if (value instanceof String){
                preparedStatement.setString(pos,(String) value);
            }else if (value instanceof Long){
                preparedStatement.setLong(pos,(Long) value);
            }else if (value instanceof Float){
                preparedStatement.setFloat(pos,(Long) value);
            }else if (value instanceof Double){
                preparedStatement.setDouble(pos,(Long) value);
            }else if (value instanceof Integer){
                preparedStatement.setInt(pos,(Integer) value);
            }else if (value instanceof java.util.Date){
                preparedStatement.setTimestamp(pos,new Timestamp(((Date) value).getTime()));
            }else if (value instanceof Boolean){
                preparedStatement.setBoolean(pos,(Boolean) value);
            }else if (value instanceof BigDecimal){
                preparedStatement.setBigDecimal(pos,(BigDecimal) value);
            }
        }catch (Exception e){
            log.error("SQL预处理失败：{}",e);
        }

    }
}
